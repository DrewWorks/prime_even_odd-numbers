
def factor_of_n(n):
    """Find the factors of a number.
       :param n where n is an integer
       :return an integer
    """
    for x in range(1, n+1):
        if n % x == 0:
            yield x
            
def is_prime(n):
    """Determine if a number is Prime or Odd.
       :param n -- an integer
       :return string
    """
    res_set = []

    if n == 0 or n == 1:
        return "0 or 1 are not prime numbers"

    if n%2 == 0:
        print("{0} is an EVEN No.".format(n))    
    else:
        for x in factor_of_n(n):
            res_set.append(x)  # append all factors of n to the list res_set              
        if len(res_set)==2 and res_set[0]==1 and res_set[1] == n: # check if length of list is 2 and if first element is 1 and last element is n
            print("{0} is a PRIME No.".format(n))
        else:
            print("{0} is an ODD No.".format(n))
    

if __name__=="__main__":
    # Usage 1
    is_prime(21)

    # Usage 2
    count = 0
    while count < 15:
        is_prime(count)
        count +=1